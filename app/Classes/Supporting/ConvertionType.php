<?php


namespace App\Classes\Supporting;


class ConvertionType {
    /**
     * @param array $array
     * @param       $obj
     * @return mixed
     */
    public static function arrayInObject(array $array, &$obj) {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $obj->$key = new \stdClass();
                self::arrayInObject($value, $obj->$key);
            }
            else {
                $obj->$key = $value;
            }
        }

        return $obj;
    }

    /**
     * @param array $array
     * @param       $object
     * @return mixed
     */
    public static function arrayToObject(array $array) {
        $object = new \stdClass();
        return self::arrayInObject($array, $object);
    }
}
