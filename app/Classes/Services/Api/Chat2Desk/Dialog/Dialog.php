<?php


namespace App\Classes\Services\Api\Chat2Desk\Dialog;


use App\Classes\Services\Api\Chat2Desk\Chat2Desk;
use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class Dialog extends Chat2Desk implements Chat2DeskInterface {

    /* last_request_id, operator_id, operator_id, channel_id, client_id, request_id */

    use FormatedClassTraids;

    /**
     * @var string
     */
    protected string $uri = '/dialogs';

    /**
     * @var string
     */
    private string $paramsUri = '';

    public function getItems() {
        $this->dialogs = [];

        $this->paramsUri = urldecode(http_build_query($this->filter));
        $dialogs         = $this->get($this->uri . ($this->paramsUri ?  ('?' . $this->paramsUri) : ''));

        if (!$this->isObject)
            return $dialogs;

        if (!isset($dialogs->data)) {
            $this->info = $dialogs;

            return $this;
        }

        $this->formatedItemsClass($dialogs, 'dialogs');

        if ($this->dialogs)
            foreach($this->dialogs as $dialog) {
                $dialog->channel = self::getManager()->channel->setId($dialog->channel_id ?? null);
                $dialog->client = self::getManager()->client->setId($dialog->client_id ?? null);
            }

        return $this;
    }

    public function getItem() {
        if (!isset($this->id))
            throw new \Exception('Not a value in id, use ->setId(id)');

        $this->dialog = NULL;

        $this->filterValidateItem();
        $uri = array_key_first($this->filter) ?? '';
        $dialog       = $this->get($this->uri . '/' . $this->id . $uri);

        if (!$this->isObject)
            return $dialog;

        $this->formatedItemClass($dialog, 'dialog', $uri);

        if ($this->dialog) {
            $this->dialog->channel = self::getManager()->channel->setId($this->dialog->channel_id ?? NULL);
            $this->dialog->client  = self::getManager()->client->setId($this->dialog->client_id ?? NULL);
        }

        return $this;
    }

    /**
     * operator_id, state
     */
    public function update() {

    }

    private function filterValidateItem() : void {
        unset($this->filter['limit'], $this->filter['offset'],
        $this->filter['operator_id'], $this->filter['order'], $this->filter['state']);
    }


    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
