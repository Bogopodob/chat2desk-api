<?php


namespace App\Classes\Services\Api\Chat2Desk\Dialog\Builder;


use App\Classes\Services\Api\Chat2Desk\Dialog\Dialog;
use App\Classes\Services\Api\Chat2Desk\Traids\FilterTraids;

class DialogBuilder implements DialogBuilderInterface {

    use FilterTraids;

    /**
     * @var array
     */
    private array $params = [];

    /**
     * @var int|null
     */
    private ?int $id = NULL;

    /**
     * @var Dialog
     */
    protected Dialog $dialog;

    public function __construct () {
        $this->create();
    }

    /**
     * @return $this|ClientBuilderInterface
     */
    public function create () : DialogBuilderInterface {
        $this->dialog = new Dialog();

        return $this;
    }

    /**
     * @param int|null $id
     */
    public function setId ($id) : self {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $method
     * @param        $data
     * @return $this
     * @throws \Exception
     */
    public function setFilter (string $method, $data = '') : self {
        $this->filterValidate($method, $data);

        return $this;
    }

    /**
     * https://documenter.getpostman.com/view/9553101/SW7gSQAM#559047c0-0123-4cca-9670-960d9cc8a2e3
     * Фильтр
     * methods:
     *   Тип данных ($data)    Метод  ($method)      Обязательные данные     Описания
     *  `int`                  **operator_id**       +Не обязательно+        - Идентификатор оператора
     *  `string`               **order**             +Не обязательно+        - Сортировка (убывания/возрастания)
     *  `string`               **state**             +Не обязательно+        - Состояние (открыт/закрыть)
     *  `int`                  **limit**             +Не обязательно+        - Количество выводимых элементов (по умолчанию 20 элементов, максимум 200)
     *  `int`                  **offset**            +Не обязательно+        - Смещения на определенное количество (по умолчанию на 0 элементов)
     *
     * @throws \Exception
     */
    private function filterValidate (string $method, $data = '') : void {
        switch ($method) {
            case 'operator_id':
            case 'limit':
            case 'offset':
            {
                $this->filter[$method] = $data;
                break;
            }
            case 'order': {
                if ($data === 'asc' || $data === 'desc') {
                    $this->filter[$method] = $data;
                    break;
                }

                throw new \Exception("The non-filtering method should be (asc or desc), $method");
            }
            case 'state': {
                if ($data === 'open' && $data === 'closed') {
                    $this->filter[$method] = $data;
                    break;
                }

                throw new \Exception("The non-filtering method should be (open or closed), $method");
            }
            default:
                throw new \Exception("Not filter method, $method");
        }
    }

    /**
     * @return Dialog
     */
    public function getDialog() : Dialog {
        $dialog = $this->dialog;

        $this->filter['limit']  = $this->limit;
        $this->filter['offset'] = $this->offset;

        $dialog->filter = $this->filter;

        if ($this->id !== NULL)
            $dialog->id = $this->id;

        $this->create();

        return $dialog;
    }
}
