<?php


namespace App\Classes\Services\Api\Chat2Desk\Dialog\Builder;


interface DialogBuilderInterface {

    /**
     * @return DialogBuilderInterface
     */
    public function create() : DialogBuilderInterface;

    /**
     * @param int $id
     * @return DialogBuilderInterface
     */
    public function setId(int $id) : DialogBuilderInterface;

    /**
     * @param int $offset
     * @return DialogBuilderInterface
     */
    public function setOffset (int $offset) : DialogBuilderInterface;

    /**
     * @param int $limit
     * @return DialogBuilderInterface
     */
    public function setLimit (int $limit): DialogBuilderInterface;
}
