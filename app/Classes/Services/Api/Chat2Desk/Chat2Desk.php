<?php


namespace App\Classes\Services\Api\Chat2Desk;


use App\Classes\Services\Api\Chat2Desk\Client\Transport;
use App\Classes\Services\Api\Chat2Desk\Factory\Chat2DeskManager;
use App\Classes\Supporting\ConvertionType;

class Chat2Desk {
    /**
     * @var string
     */
    protected static $token = '';

    /**
     * @var string
     */
    protected static $xBotMarketing = '';

    /**
     * @var string
     */
    protected static $baseUrl = 'https://api.chat2desk.com';

    /**
     * @var int
     */
    protected static $version = 1;

    /**
     * @var int
     */
    protected int $timeout = 10;

    /**
     * @var string
     */
    protected string $uri = '';

    /**
     * @var null
     */
    protected $info = NULL;

    /**
     * @var bool
     */
    protected bool $isObject = TRUE;

    /**
     * @param string $token
     */
    public static function setToken (string $token) {
        self::$token = $token;
    }

    /**
     * @param string $xBotMarketing
     */
    public static function setXBotMarketing (string $xBotMarketing) {
        self::$xBotMarketing = $xBotMarketing;
    }

    /**
     * @param int $version
     */
    public static function setVersion (int $version) {
        self::$version = $version;
    }

    /**
     * @return Chat2DeskManager
     */
    public static function getManager () {
        return new Chat2DeskManager();
    }

    /**
     * Запрос в chat2desk
     *
     * @param string $method
     * @param array  $params
     * @return array|mixed
     * @throws \Exception
     */
    private function send (string $uri, string $method = 'get', array $params = []) {
        if (!self::$token)
            throw new \Exception('Not token');

        $curl = curl_init();

        $url = self::$baseUrl . '/' . 'v' . self::$version . $uri;

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
        if ($method === 'post') {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
        }

        else if ($method === 'put') {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Authorization:' . self::$token,
                'Content-Type:application/json',
                'X-BotMarketing:' . self::$xBotMarketing
            ]);

        $curlData = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        $data = json_decode($curlData, TRUE);

        if ($httpCode === 200)
            return $data;

        else if ($httpCode === 400)
            throw new \Exception("Bad request. url: $url || params: " . implode(', ', $params));

        else if ($httpCode === 401)
            throw new \Exception("Unauthorized. url: $url ||" . 'token: ' . self::$token);

        else if ($httpCode === 403)
            throw new \Exception('Forbidden');

        else if ($httpCode === 404)
            throw new \Exception("Not found. url: $url");

        else if ($httpCode === 500)
            throw new \Exception('Error');

        throw new \Exception('Unexpected error');
    }

    /**
     * Get запрос, $object - работать как с объектом или массивом
     *
     * @param string $uri
     * @param null   $object
     * @return array|object
     * @throws \Exception
     */
    protected function get (string $uri) {
        $data = $this->send($uri, 'get');
        if ($this->isObject)
            return ConvertionType::arrayToObject($data);

        return $data;
    }

    /**
     * @param string $uri
     * @param array  $params
     * @return array|mixed
     * @throws \Exception
     */
    protected function put (string $uri, array $params) {
        $data = $this->send($uri, 'put', $params);
        if ($this->isObject)
            return ConvertionType::arrayToObject($data);

        return $data;
    }

    /**
     * @param string $uri
     * @param array  $params
     * @return array|mixed
     * @throws \Exception
     */
    protected function post (string $uri, array $params) {
        $data = $this->send($uri, 'post', $params);
        if ($this->isObject)
            return ConvertionType::arrayToObject($data);

        return $data;
    }
}
