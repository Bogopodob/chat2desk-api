<?php


namespace App\Classes\Services\Api\Chat2Desk\Companies\Builder;


use App\Classes\Services\Api\Chat2Desk\Companies\Companies;

class CompanyBuilder implements CompanyBuilderInterface {

    /**
     * @var array|string[]
     */
    private static array $uris = ['/api_info', '/switch_mode'];

    /**
     * @var array
     */
    private array $filter = [];

    /**
     * @var array
     */
    private array $params = [];

    /**
     * @var Companies
     */
    protected Companies $company;

    public function __construct () {
        $this->create();
    }

    /**
     * @return $this
     */
    public function create () : self {
        $this->company = new Companies();
        return $this;
    }

    /**
     * @param string $method
     * @param        $data
     * @return $this
     * @throws \Exception
     */
    public function setFilter (string $method, $data = '') : self {
        $this->filterValidate($method, $data);

        return $this;
    }

    /**
     * https://documenter.getpostman.com/view/9553101/SW7gSQAM#84e61c7c-3111-4019-87e9-c87054198aea
     * Фильтр
     * methods:
     *   Тип данных ($data)   Метод  ($method)      Обязательные данные         Описания
     *  `string`              **api_info**          +Не обязательно+            - Получение текущей версии API,
     *                                                                          уровень доступа, количество запросов,
     *                                                                          информацию о компании, доменные адреса
     *                                                                          сервиса, количество каналов, язык использования.
     *
     * @throws \Exception
     */
    private function filterValidate (string $method, $data = '') : void {
        switch ($method) {
            case 'api_info':
            case 'switch_mode':
            {
                $this->filter['/' . $method] = $data;
                break;
            }
            default:
                throw new \Exception("Not filter method, $method");
        }

        if (isset($filter['/api_info']) && isset($filter['switch_mode']))
            throw new \Exception("You cannot use these methods in place, (api_info or switch_mode)");
    }

    /**
     * @return Companies
     */
    public function getCompany() {
        $company = $this->company;
        $company->filter = $this->filter;
        $company->uris = self::$uris;
        $company->params = $this->params;

        $this->create();

        return $company;
    }
}
