<?php


namespace App\Classes\Services\Api\Chat2Desk\Companies\Builder;


interface CompanyBuilderInterface {
    public function create() : CompanyBuilderInterface;
}
