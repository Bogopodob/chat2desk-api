<?php


namespace App\Classes\Services\Api\Chat2Desk\Companies;


use App\Classes\Services\Api\Chat2Desk\Chat2Desk;
use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class Companies extends Chat2Desk implements Chat2DeskInterface {
    use FormatedClassTraids;

    /**
     * @var string
     */
    protected string $uri = '/companies';

    /**
     * @var string
     */
    private string $paramsUri = '';

    public function getItems() {
        $this->company = [];

        $uri = array_key_first($this->filter) ?? '';
        $company         = $this->get($this->uri . $uri);

        if (!$this->isObject)
            return $company;

        if (!isset($company->data)) {
            $this->info = $company;

            return $this;
        }

        return $this->formatedItemsClass($company, 'company');
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
