<?php


namespace App\Classes\Services\Api\Chat2Desk\Lists\Contact;


use App\Classes\Services\Api\Chat2Desk\Chat2Desk;
use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Lists\ListsTrait;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class Region extends Chat2Desk implements Chat2DeskInterface {

    use ListsTrait, FormatedClassTraids;

    /**
     * @var string
     */
    protected string $uri = '/regions';

    /**
     * @return $this|array
     * @throws \Exception
     */
    public function getItems() {
        $this->regions = [];

        $this->paramsUri = urldecode(http_build_query($this->filter));
        $regions       = $this->get($this->paramsUri ? $this->uri . '?' . $this->paramsUri : $this->uri);

        if (is_array($regions))
            return $regions;

        if (!isset($regions->data)) {
            $this->info = $regions;
            return $this;
        }

        return $this->formatedItemsClass($regions, 'regions');
    }

    /**
     * @param int|int $id
     * @return Region|array
     */
    public function getItem() {
        if (!isset($this->id))
            throw new \Exception('Not a value in id, use ->setId(id)');

        $this->region = NULL;

        if (!$this->maxRequest)
            $this->maxRequest = 30;

        if (!$this->id || !$region = $this->find('region', $this->maxRequest))
            return $this->isObject ? $this : ['country' => null, 'meta' => null, 'status' => 'success'];

        return $region;
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
