<?php


namespace App\Classes\Services\Api\Chat2Desk\Lists\Contact;


use App\Classes\Services\Api\Chat2Desk\Chat2Desk;
use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Lists\ListsTrait;

class Country extends Chat2Desk implements Chat2DeskInterface {

    use ListsTrait;

    /**
     * @var string
     */
    protected string $uri = '/countries';

    /**
     * @return $this|array
     * @throws \Exception
     */
    public function getItems () {
        $this->countries = [];

        $this->paramsUri = urldecode(http_build_query($this->filter));
        $countries       = $this->get($this->paramsUri ? $this->uri . '?' . $this->paramsUri : $this->uri);

        if (is_array($countries))
            return $countries;

        if (!isset($countries->data)) {
            $this->info = $countries;
            return $this;
        }

        $this->countries = $countries->data;

        unset($countries->data);
        $this->info      = $countries;
        $this->info->url = $this::$baseUrl . $this->uri . ($this->paramsUri ? '?' . $this->paramsUri : '');
        unset($countries);

        return $this;
    }

    /**
     * @return Country|ListsTrait|array|null
     */
    public function getItem () {
        if (!isset($this->id))
            throw new \Exception('Not a value in id, use ->setId(id)');

        $this->country = NULL;

        if (!isset($this->id))
            return $this->isObject ? $this : ['country' => null, 'meta' => null, 'status' => 'success'];

        if (!$this->maxRequest)
            $this->maxRequest = 5;

        if (!$country = $this->find('country', $this->maxRequest ?: 5))
            return $this->isObject ? $this : ['country' => null, 'meta' => null, 'status' => 'success'];

        return $country;
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
