<?php


namespace App\Classes\Services\Api\Chat2Desk\Lists\Contact\Builder;


use App\Classes\Services\Api\Chat2Desk\Lists\Contact\Country;
use App\Classes\Services\Api\Chat2Desk\Traids\FilterTraids;

class CountryBuilder implements CountryBuilderInterface {

    use FilterTraids;

    /**
     * @var int|null
     */
    private ?int $id = NULL;

    /**
     * @var Country
     */
    protected Country $country;

    /**
     * CountryBuilder constructor.
     */
    public function __construct () {
        $this->create();
    }

    /**
     * @return $this|CountryBuilderInterface
     */
    public function create () : CountryBuilderInterface {
        $this->country = new Country();

        return $this;
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId (?int $id) : self {
        $this->id = $id;

        return $this;
    }

    /**
     * @param int $maxRequest
     * @return $this
     */
    public function setMaxRequest(int $maxRequest) : self {
        $this->country->maxRequest = $maxRequest;

        return $this;
    }

    /**
     * @return Country
     */
    public function getCountry () : Country {
        $country         = $this->country;
        $this->filter    = ['limit' => $this->limit, 'offset' => $this->offset];
        $country->filter = $this->filter;

        if ($this->id !== null)
            $country->id     = $this->id;

        $this->create();

        return $country;
    }
}
