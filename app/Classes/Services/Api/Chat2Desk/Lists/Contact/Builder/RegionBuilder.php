<?php


namespace App\Classes\Services\Api\Chat2Desk\Lists\Contact\Builder;


use App\Classes\Services\Api\Chat2Desk\Lists\Contact\Region;
use App\Classes\Services\Api\Chat2Desk\Traids\FilterTraids;

class RegionBuilder implements RegionBuilderInterface {

    use FilterTraids;

    /**
     * @var int|null
     */
    private ?int $id = NULL;

    /**
     * @var Region
     */
    protected Region $region;

    /**
     * CountryBuilder constructor.
     */
    public function __construct () {
        $this->create();
    }

    /**
     * @return $this|RegionBuilderInterface
     */
    public function create () : RegionBuilderInterface {
        $this->region = new Region();

        return $this;
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId (?int $id) : self {
        $this->id = $id;

        return $this;
    }

    /**
     * @param int $maxRequest
     * @return $this
     */
    public function setMaxRequest (int $maxRequest) : self {
        $this->region->maxRequest = $maxRequest;

        return $this;
    }

    /**
     * @return Region
     */
    public function getRegion () : Region {
        $region         = $this->region;
        $this->filter   = ['limit' => $this->limit, 'offset' => $this->offset];
        $region->filter = $this->filter;

        if ($this->id !== null)
            $region->id     = $this->id;

        $this->create();

        return $region;
    }
}
