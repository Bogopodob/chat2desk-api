<?php


namespace App\Classes\Services\Api\Chat2Desk\Lists\Contact\Builder;


interface CountryBuilderInterface {

    /**
     * @return CountryBuilderInterface
     */
    public function create() : CountryBuilderInterface;

    /**
     * @param int|null $id
     * @return CountryBuilderInterface
     */
    public function setId(?int $id) : CountryBuilderInterface;

    /**
     * @return CountryBuilderInterface
     */
    public function setMaxRequest(int $maxRequest) : CountryBuilderInterface;

    /**
     * @param int $limit
     * @return CountryBuilderInterface
     */
    public function setLimit(int $limit) : CountryBuilderInterface;

    /**
     * @param int $offset
     * @return CountryBuilderInterface
     */
    public function setOffset(int $offset) : CountryBuilderInterface;
}
