<?php


namespace App\Classes\Services\Api\Chat2Desk\Lists\Contact\Builder;


interface RegionBuilderInterface {

    /**
     * @return RegionBuilderInterface
     */
    public function create() : RegionBuilderInterface;

    /**
     * @param int|null $id
     * @return RegionBuilderInterface
     */
    public function setId(?int $id) : RegionBuilderInterface;

    /**
     * @param int $maxRequest
     * @return RegionBuilderInterface
     */
    public function setMaxRequest(int $maxRequest) : RegionBuilderInterface;

    /**
     * @param int $limit
     * @return RegionBuilderInterface
     */
    public function setLimit(int $limit) : RegionBuilderInterface;

    /**
     * @param int $offset
     * @return RegionBuilderInterface
     */
    public function setOffset(int $offset) : RegionBuilderInterface;
}
