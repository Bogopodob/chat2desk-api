<?php


namespace App\Classes\Services\Api\Chat2Desk\Lists;


trait ListsTrait {

    /**
     * @var string
     */
    private $paramsUri = '';

    /**
     * @var int
     */
    public int $maxRequest = 0;

    /**
     * @param string    $key
     * @param int       $id
     * @param bool|bool $isSort
     * @param int|int   $range
     * @return ListsTrait|array|null
     */
    private function find (string $key, int $range = 5) {
        $count = 0;
        while (TRUE) {
            if ($count >= $range)
                return NULL;

            $this->paramsUri = urldecode(http_build_query($this->filter));
            $items           = $this->get($this->paramsUri ? $this->uri . '?' . $this->paramsUri : $this->uri);

            $total = 0;

            if (!$this->isObject) {
                $total = $items['meta']['total'] ?? 0;

                if ($item = $this->findInArray($key, $items, $count))
                    return $item;
            }
            else {
                $total = $items->meta->total ?? 0;

                if ($item = $this->findInObject($key, $items, $count))
                    return $item;
            }

            if ($total === 0 || $this->filter['offset'] >= $total)
                return NULL;

            $count++;

            $this->filter['offset'] += 200;
        }
        return NULL;
    }

    /**
     * @param string    $key
     * @param \stdClass $items
     */
    private function findInObject (string $key, \stdClass $items, int $count) : ?self {
        if ($item = array_filter(isset($items->data) ? (array)$items->data : [], function ($e) {
            return $e->id === $this->id;
        })) {
            unset($items->data, $items->meta);
            $this->$key         = $item[array_key_first($item)] ?? NULL;
            $this->info         = $items ?? NULL;
            $this->info->id     = $this->id;
            $this->countRequest = $count ?: 1;
            $this->info->url    = $this::$baseUrl . $this->uri . ($this->paramsUri ? '?' . $this->paramsUri : '');
            unset($items);

            return $this;
        }

        return NULL;
    }

    /**
     * @param string $key
     * @param array  $items
     * @param int    $count
     * @return array|null
     */
    private function findInArray (string $key, array $items, int $count) : ?array {
        if ($item = array_filter(isset($items['data']) ? (array)$items['data'] : [], function ($e) {
            return $e['id'] === $this->id;
        }))
            return [
                $key            => $item[array_key_first($item)] ?? NULL,
                'meta'          => $items['meta'],
                'status'        => $items['status'],
                'count_request' => $count ?: 1,
            ];

        return NULL;
    }
}
