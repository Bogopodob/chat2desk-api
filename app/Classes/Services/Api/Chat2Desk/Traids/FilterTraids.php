<?php


namespace App\Classes\Services\Api\Chat2Desk\Traids;


trait FilterTraids {

    /**
     * @var array
     */
    private array $filter = [];

    /**
     * @var int
     */
    private int $limit = 200;

    /**
     * @var int
     */
    private int $offset = 0;

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit (int $limit) : self {
        if ($this->limit)
            $this->limit = $limit;

        return $this;
    }

    /**
     * @param int $offset
     * @return $this
     */
    public function setOffset (int $offset) : self {
        $this->offset = $offset;

        return $this;
    }
}
