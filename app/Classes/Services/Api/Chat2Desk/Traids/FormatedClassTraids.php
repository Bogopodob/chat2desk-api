<?php


namespace App\Classes\Services\Api\Chat2Desk\Traids;


trait FormatedClassTraids {

    /**
     * @param \stdClass $items
     * @param string    $key
     * @return $this
     */
    private function formatedItemsClass(\stdClass $items, string $key) {
        if (!isset($items->data)) {
            $this->info = $items;

            return $this;
        }

        $this->$key = $items->data;
        unset($items->data);

        $this->info      = $items;
        $this->info->url = self::$baseUrl . '/v' . self::$version . $this->uri . ($this->paramsUri ? '?' . $this->paramsUri : '');
        $this->info->method = 'get';
        unset($items);

        return $this;
    }

    /**
     * @param \stdClass $item
     * @param string    $key
     * @param string    $uri
     * @return $this
     */
    private function formatedItemClass(\stdClass $item, string $key, string $uri = '') {
        if (!isset($item->data)) {
            $this->info = $item;

            return $this;
        }

        $this->$key = $item->data;
        unset($item->data);

        $this->info = $item;
        $this->info->url = $this::$baseUrl . $this->uri . '/' . $this->id . $uri;
        $this->info->method = 'get';
        unset($item);

        return $this;
    }
}
