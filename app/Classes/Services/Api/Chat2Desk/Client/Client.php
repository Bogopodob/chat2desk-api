<?php


namespace App\Classes\Services\Api\Chat2Desk\Client;


use App\Classes\Services\Api\Chat2Desk\Chat2Desk;
use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Lists\Contact\Builder\CountryBuilder;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;
use App\Classes\Supporting\ConvertionType;

class Client extends Chat2Desk implements Chat2DeskInterface {

    use FormatedClassTraids;

    /**
     * @var string
     */
    protected string $uri = '/clients';

    /**
     * @var string
     */
    private string $paramsUri = '';

    /**
     * @param bool|bool $isResultObject
     * @return $this|array
     * @throws \Exception
     */
    public function getItems () {
        $this->clients = [];

        $this->paramsUri = urldecode(http_build_query($this->filter));
        $clients         = $this->get($this->uri . '?' . $this->paramsUri);

        if (!$this->isObject)
            return $clients;

        if (!isset($clients->data)) {
            $this->info = $clients;

            return $this;
        }

        $this->formatedItemsClass($clients, 'clients');

        foreach ($this->clients as $client) {
            if (isset($client->country_id))
                $client->country = self::getManager()->country->setId($client->country_id);

            if (isset($client->region_id))
                $client->region = self::getManager()->region->setId($client->region_id);
        }

        if (isset($this->clients->channels))
            foreach ($this->clients->channels as $channel)
                if (isset($channel->id))
                    $channel->channel = self::getManager()->channel->setId($channel->id);

        return $this;
    }

    /**
     * @param int       $id
     * @param bool|bool $isResultObject
     * @return $this|array
     * @throws \Exception
     */
    public function getItem () {
        if (!isset($this->id))
            throw new \Exception('Not a value in id, use ->setId(id)');

        $this->client = NULL;

        $this->filterValidateItem();
        $uri = array_key_first($this->filter) ?? '';
        $client       = $this->get($this->uri . '/' . $this->id . $uri);

        if (!$this->isObject)
            return $client;

        $this->formatedItemClass($client, 'client', $uri);

        if ($uri === '/transport') {
            if (!$this->client)
                return $this;

            foreach($this->client as $channel)
                if (isset($channel->channel_id))
                    $channel->channel = self::getManager()->channel->setId($channel->channel_id);
        }

        if (isset($this->client->country_id))
            $this->client->country = self::getManager()->country->setId($this->client->country_id);

        if (isset($this->client->region_id))
            $this->client->region  = self::getManager()->country->setId($this->client->region_id);

        if (isset($this->client->channels))
            foreach ($this->client->channels as $channel)
                if (isset($channel->id))
                    $channel->channel = self::getManager()->channel->setId($channel->id);

        return $this;
    }

    public function update() {
        if (!isset($this->id))
            throw new \Exception('Not a value in id, use ->setId(id)');

        $this->filterValidateItem();
        $this->filterValidateItems();

        $this->client = NULL;

        $client       = $this->put($this->uri . '/' . $this->id, $this->params);

        return $this;
    }

    public function create() {
        $this->filterValidateItem();
        $this->filterValidateItems();

        $this->client = NULL;

        $client       = $this->post($this->uri, $this->params);
        return $this;
    }


    private function filterValidateItem() : void {
        unset($this->filter['limit'], $this->filter['offset'],
            $this->filter['phone'], $this->filter['tags'], $this->filter['created_after']);
    }

    private function filterValidateItems() : void {
        unset($this->filter['transport'], $this->filter['last_question'],
        $this->filter['dialogs'], $this->filter['questions']);
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
