<?php


namespace App\Classes\Services\Api\Chat2Desk\Client\Builder;


interface ClientBuilderInterface {
    /**
     * @return ClientBuilderInterface
     */
    public function create () : ClientBuilderInterface;

    /**
     * @param int $id
     * @return ClientBuilderInterface
     */
    public function setId(int $id) : ClientBuilderInterface;

    /**
     * @param int $offset
     * @return ClientBuilderInterface
     */
    public function setOffset (int $offset) : ClientBuilderInterface;

    /**
     * @param int $limit
     * @return ClientBuilderInterface
     */
    public function setLimit (int $limit): ClientBuilderInterface;

    /**
     * @param string|null $comment
     * @return ClientBuilderInterface
     */
    public function setComment(?string $comment) : ClientBuilderInterface;

    /**
     * @param string|null $name
     * @return ClientBuilderInterface
     */
    public function setName(?string $name) : ClientBuilderInterface;

    public function setPhone(string $phone) : ClientBuilderInterface;
//
//    public function setAvatar(?string $avatar) : ClientBuilderInterface;
//
//    public function setCountryId(int $id) : ClientBuilderInterface;
//
//    public function setRegionId(?int $id) : ClientBuilderInterface;
}
