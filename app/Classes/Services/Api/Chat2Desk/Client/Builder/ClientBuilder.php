<?php


namespace App\Classes\Services\Api\Chat2Desk\Client\Builder;


use App\Classes\Services\Api\Chat2Desk\Client\Client;
use App\Classes\Services\Api\Chat2Desk\Traids\FilterTraids;
use App\Classes\Services\Api\Chat2Desk\Transport;

class ClientBuilder implements ClientBuilderInterface {

    use FilterTraids;

    /**
     * @var array|string[]
     */
    private static array $uris = ['/transport', '/last_question', '/dialogs', '/questions'];

    /**
     * @var array
     */
    private array $params = [];

    /**
     * @var int|null
     */
    private ?int $id = NULL;

    /**
     * @var Client
     */
    protected Client $client;

    /**
     * ClientBuilder constructor.
     */
    public function __construct () {
        $this->create();
    }

    /**
     * @return $this|ClientBuilderInterface
     */
    public function create () : ClientBuilderInterface {
        $this->client = new Client();

        return $this;
    }

    /**
     * @param int|null $id
     * @return $this|ClientBuilderInterface
     */
    public function setId (?int $id) : ClientBuilderInterface {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $method
     * @param        $data
     * @return $this
     * @throws \Exception
     */
    public function setFilter (string $method, $data = '') : self {
        $this->filterValidate($method, $data);

        return $this;
    }

    /**
     * https://documenter.getpostman.com/view/9553101/SW7gSQAM#30a38579-20e6-40e3-8de8-698de878f2a3
     * Фильтр
     * methods:
     *   Тип данных ($data)   Метод  ($method)      Обязательные данные           Описания
     *  `string`              **last_question**     +Не обязательно+        - Последнее сообщение
     *  `string`              **dialogs**           +Не обязательно+        - Каналы диалогов
     *  `string`              **transport**         +Не обязательно+        - Каналы диалогов
     *  `array|string`        **questions**         +Не обязательно+        - Сообщения
     *  `string`              **phone**             +Не обязательно+        - Номер телефона
     *  `array|string`        **tags**              +Не обязательно+        - Теги
     *  `string`              **created_after**     +Не обязательно+        - Дата создания
     *  `string`              **order**             +Не обязательно+        - Сотрировка (asc, desc)
     *  `int`                 **limit**             +Не обязательно+        - Количество выводимых элементов (по умолчанию 20 элементов, максимум 200)
     *  `int`                 **offset**            +Не обязательно+        - Смещения на определенное количество (по умолчанию на 0 элементов)
     *
     * @throws \Exception
     */
    private function filterValidate (string $method, $data = '') : void {
        switch ($method) {
            case 'created_after':
            case 'limit':
            case 'offset':
            case 'phone':
            {
                $this->filter[$method] = $data;
                break;
            }
            case 'order': {
                if ($data === 'asc' || $data === 'desc') {
                    $this->filter[$method] = $data;
                    break;
                }

                throw new \Exception("The non-filtering method should be (asc or desc), $method");
            }
            case 'last_question':
            case 'dialogs':
            case 'transport':
            {
                $this->filter['/' . $method] = '';
                break;
            }
            case 'tags':
            {
                if (is_string($data) || is_array($data)) {
                    $this->filter[$method] = is_string($data) ? $data : implode(',', $data);
                    break;
                }

                throw new \Exception('Invalid arguments in variable ($data), must be a string (1,2,3) or an array ([1, 2, 3])');
            }
            case 'questions':
            {
                if (!is_array($data))
                    throw new \Exception('The variable ($data) must be an array');

                if (!isset($data['start_date']) && !isset($data['finish_date']))
                    throw new \Exception('The variable ($data) must have at least one of the array keys (["start_date" => "10-09-2011", "finish_date" => "10-09-2019"])');

                $this->filter['/' . $method] = $data;
                break;
            }
            default:
                throw new \Exception("Not filter method, $method");
        }

        $isRepeatUri = FALSE;
        foreach (self::$uris as $u)
            if (array_key_exists($u, $this->filter)) {
                if (!$isRepeatUri)
                    $isRepeatUri = TRUE;

                else
                    throw new \Exception('Нельзя использовать несколько видов фильтров, в фильтре должен быть только один из \'/transport\', \'/last_question\', \'/dialogs\', \'/questions\'');
            }
    }

    /**
     * @param string|null $comment
     * @return $this
     */
    public function setComment(?string $comment) : self {
        $this->params['comment'] = $comment;

        return $this;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name) : self {
        $this->params['name'] = $name;

        return $this;
    }

    public function setPhone (string $phone) : ClientBuilderInterface {
        $this->params['phone'] = $phone;

        return $this;
    }

    /**
     * @return Client
     */
    public function getClient () : Client {
        $client = $this->client;

        $this->filter['limit']  = $this->limit;
        $this->filter['offset'] = $this->offset;
        $client->filter         = $this->filter;

        $client->params = $this->params;

        $client->uris = self::$uris;

        if ($this->id !== NULL)
            $client->id = $this->id;

        $this->create();

        return $client;
    }
}
