<?php


namespace App\Classes\Services\Api\Chat2Desk;


interface Chat2DeskInterface {

    /**
     * @param bool $isObject
     * @return mixed
     */
    public function toObject(bool $isObject);
}
