<?php


namespace App\Classes\Services\Api\Chat2Desk\Message;


use App\Classes\Services\Api\Chat2Desk\Chat2Desk;
use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class Message extends Chat2Desk implements Chat2DeskInterface {

    /* operator_id, request_id */

    use FormatedClassTraids;

    /**
     * @var string
     */
    protected string $uri = '/messages';

    /**
     * @var string
     */
    private string $paramsUri = '';


    public function getItems() {
        $this->messages = [];

        $this->paramsUri = urldecode(http_build_query($this->filter));
        $messages         = $this->get($this->paramsUri ? $this->uri . '?' . $this->paramsUri : $this->uri);

        if (!$this->isObject)
            return $messages;

        $this->formatedItemsClass($messages, 'messages');
        if ($this->messages)
            foreach($this->messages as $message) {
                $message->dialog = self::getManager()->dialog->setId($message->dialog_id ?? null);
                $message->channel = self::getManager()->channel->setId($message->channel_id ?? null);
                $message->client = self::getManager()->client->setId($message->client_id ?? null);
            }

        return $this;
    }

    public function getItem() {
        if (!isset($this->id))
            throw new \Exception('Not a value in id, use ->setId(id)');

        unset($this->filter);
        $this->message = NULL;
        $message       = $this->get($this->uri . '/' . $this->id);
        if (is_array($message))
            return $message;

        $this->formatedItemClass($message, 'message');

        if ($this->message) {
            $this->message->dialog  = self::getManager()->dialog->setId($this->message->dialog_id ?? NULL);
            $this->message->channel = self::getManager()->channel->setId($this->message->channel_id ?? NULL);
            $this->message->client  = self::getManager()->client->setId($this->message->client_id ?? NULL);
        }

        return $this;
    }

    public function create() {

    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
