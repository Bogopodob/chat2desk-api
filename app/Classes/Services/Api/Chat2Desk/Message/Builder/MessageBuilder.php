<?php


namespace App\Classes\Services\Api\Chat2Desk\Message\Builder;


use App\Classes\Services\Api\Chat2Desk\Message\Message;
use App\Classes\Services\Api\Chat2Desk\Traids\FilterTraids;

class MessageBuilder implements MessageBuilderInterface {

    use FilterTraids;

    /**
     * @var int|null
     */
    private ?int $id = NULL;

    protected Message $message;

    public function __construct () {
        $this->create();
    }

    public function create () : self {
        $this->message = new Message();

        return $this;
    }

    /**
     * @param int|null $id
     */
    public function setId (?int $id) : self {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $method
     * @param string $data
     * @return $this
     * @throws \Exception
     */
    public function setFilter (string $method, $data = '') : self {
        $this->filterValidate($method, $data);

        return $this;
    }

    /**
     * https://documenter.getpostman.com/view/9553101/SW7gSQAM#2c39ddf1-0b8f-4f97-b1e7-0c76801cac82
     * Фильтр
     * methods:
     *   Тип данных      Метод                 Обязательные данные           Описания
     *  `int`            **limit**             +Не обязательно+        - Количество выводимых элементов (по умолчанию 20 элементов, максимум 200)
     *  `int`            **offset**            +Не обязательно+        - Смещения на определенное количество (по умолчанию на 0 элементов)
     *  `string`         **transport**         +Не обязательно+        - Названия мессенджера
     *  `int`            **channel_id**        +Не обязательно+        - Идентификатор канала
     *  `int`            **client_id**         +Не обязательно+        - Идентификатор клиента
     *  `int`            **dialog_id**         +Не обязательно+        - Идентификатор диалога
     *  `int`            **operator_id**       +Не обязательно+        - Идентификатор оператора
     *  `int`            **read**              +Не обязательно+        - Было ли просмотрено сообщение
     *  `int`            **start_id**          +Не обязательно+        - Идентификатор сообщения, начиная с определенного сообщения (id)
     *  `int|string`     **start_date**        +Не обязательно+        - Дата начала
     *  `int|string`     **finish_date**       +Не обязательно+       - Дата окончания
     *
     * @throws \Exception
     */
    private function filterValidate (string $method, $data = '') : void {
        switch ($method) {
            case 'limit':
            case 'offset':
            case 'transport':
            case 'channel_id':
            case 'client_id':
            case 'dialog_id':
            case 'read':
            case 'operator_id':
            case 'start_id':
            {
                $this->filter[$method] = $data;
                break;
            }
            case 'order':
            {
                if ($data === 'asc' || $data === 'desc') {
                    $this->filter[$method] = $data;
                    break;
                }

                throw new \Exception("The non-filtering method should be (asc or desc), $method");
            }
            case 'finish_date':
            case 'start_date': {
                if (is_int($data)) {
                    $this->filter[$method] = \date('d-m-Y', strtotime($data));
                    break;
                }

                else if (is_string($data)) {
                    if (!preg_match('/(?:\d{4}|\d{2})(?:\.|\-|\/)\d{2}(?:\.|\-|\/)(?:\d{4}|\d{2})/ui', $data))
                        throw new \Exception("This method has an unacceptable date format, for example: (" . \date('Y-m-d') . ' or ' . \date('d-m-Y') . "). Symbols: (-|.|/), $method");

                    $this->filter[$method] = $data;
                    break;
                }

                throw new \Exception('The value must be a string or a number.');
            }
            default:
                throw new \Exception('Not filter method');
        }
    }

    /**
     * @return Message
     */
    public function getMessage () : Message {
        $message = $this->message;

        $this->filter['limit']  = $this->limit;
        $this->filter['offset'] = $this->offset;
        $message->filter        = $this->filter;

        if ($this->id !== NULL)
            $message->id = $this->id;

        $this->create();

        return $message;
    }
}
