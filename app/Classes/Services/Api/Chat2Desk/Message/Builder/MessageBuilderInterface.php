<?php


namespace App\Classes\Services\Api\Chat2Desk\Message\Builder;


interface MessageBuilderInterface {

    /**
     * @return MessageBuilderInterface
     */
    public function create() : MessageBuilderInterface;

    /**
     * @param int|null $id
     * @return MessageBuilderInterface
     */
    public function setId(?int $id) : MessageBuilderInterface;

    /**
     * @param int $offset
     * @return MessageBuilderInterface
     */
    public function setOffset(int $offset) : MessageBuilderInterface;

    /**
     * @param int $limit
     * @return MessageBuilderInterface
     */
    public function setLimit(int $limit) : MessageBuilderInterface;

    /**
     * @param string $method
     * @param        $data
     * @return MessageBuilderInterface
     */
    public function setFilter(string $method, $data) : MessageBuilderInterface;
}
