<?php


namespace App\Classes\Services\Api\Chat2Desk\Channel;


use App\Classes\Services\Api\Chat2Desk\Chat2Desk;
use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class Channel extends Chat2Desk implements Chat2DeskInterface {

    use FormatedClassTraids;

    /**
     * @var string
     */
    protected string $uri = '/channels';

    /**
     * @var string
     */
    protected string $paramsUri = '';

    /**
     * @return Channel|array|object
     * @throws \Exception
     */
    public function getItems() {
        $this->channels = [];

        $this->paramsUri = urldecode(http_build_query($this->filter));
        $channels         = $this->get($this->paramsUri ? $this->uri . '?' . $this->paramsUri : $this->uri);

        if (!$this->isObject)
            return $channels;

        return $this->formatedItemsClass($channels, 'channels');
    }

    /**
     * @return Channel|array|object
     * @throws \Exception
     */
    public function getItem() {
        if (!isset($this->id))
            throw new \Exception('Not a value in id, use ->setId(id)');

        unset($this->filter);
        $this->channel = NULL;
        $channel       = $this->get($this->uri . '/' . $this->id);
        if (!$this->isObject)
            return $channel;

        return $this->formatedItemClass($channel, 'channel');
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
