<?php


namespace App\Classes\Services\Api\Chat2Desk\Channel\Builder;


interface ChannelBuilderInterface {

    /**
     * @return ChannelBuilderInterface
     */
    public function create() : ChannelBuilderInterface;

    /**
     * @param int|null $id
     * @return ChannelBuilderInterface
     */
    public function setId(?int $id) : ChannelBuilderInterface;

    /**
     * @param int $offset
     * @return ChannelBuilderInterface
     */
    public function setOffset(int $offset) : ChannelBuilderInterface;

    /**
     * @param int $limit
     * @return ChannelBuilderInterface
     */
    public function setLimit(int $limit) : ChannelBuilderInterface;

    /**
     * @param string $method
     * @param        $data
     * @return ChannelBuilderInterface
     */
    public function setFilter(string $method, $data) : ChannelBuilderInterface;
}
