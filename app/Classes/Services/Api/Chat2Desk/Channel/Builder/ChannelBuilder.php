<?php


namespace App\Classes\Services\Api\Chat2Desk\Channel\Builder;


use App\Classes\Services\Api\Chat2Desk\Channel\Channel;
use App\Classes\Services\Api\Chat2Desk\Traids\FilterTraids;

class ChannelBuilder implements ChannelBuilderInterface {
    use FilterTraids;

    /**
     * @var int|null
     */
    private ?int $id = null;

    /**
     * @var Channel
     */
    protected Channel $channel;

    public function __construct () {
       $this->create();
    }

    /**
     * @return $this|ChannelBuilderInterface
     */
    public function create() : self {
        $this->channel = new Channel();

        return $this;
    }

    /**
     * @param int|null $id
     * @return $this|ChannelBuilderInterface
     */
    public function setId (?int $id) : ChannelBuilderInterface {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $method
     * @param string $data
     * @return $this
     * @throws \Exception
     */
    public function setFilter(string $method, $data = '') : self {
        $this->filterValidate($method, $data);

        return $this;
    }

    /**
     * https://documenter.getpostman.com/view/9553101/SW7gSQAM#2c39ddf1-0b8f-4f97-b1e7-0c76801cac82
     * Фильтр
     * methods:
     *   Тип данных   Метод                 Обязательные данные           Описания
     *  `string`      **phone**             +Не обязательно+        - Номер телефона
     *  `int`         **limit**             +Не обязательно+        - Количество выводимых элементов (по умолчанию 20 элементов, максимум 200)
     *  `int`         **offset**            +Не обязательно+        - Смещения на определенное количество (по умолчанию на 0 элементов)
     *
     * @throws \Exception
     */
    private function filterValidate(string $method, $data = '') : void {
        switch ($method) {
            case 'limit':
            case 'offset':
            case 'phone':
            {
                $this->filter[$method] = $data;
                break;
            }
            default:
                throw new \Exception('Not filter method');
        }
    }

    /**
     * @return Channel
     */
    public function getChannel() : Channel {
        $channel         = $this->channel;

        $this->filter['limit'] = $this->limit;
        $this->filter['offset'] = $this->offset;
        $channel->filter = $this->filter;

        if ($this->id !== null)
            $channel->id     = $this->id;

        $this->create();

        return $channel;
    }
}
