<?php


namespace App\Classes\Services\Api\Chat2Desk\Help\Roles;


use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Help\Help;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class Roles extends Help implements Chat2DeskInterface {
    use FormatedClassTraids;

    protected string $uri = '/roles';

    /**
     * @var string
     */
    private string $paramsUri = '';

    public function getRoles() {
        $this->roles = [];

        $this->uri = self::$baseUri . $this->uri;

        $roles = $this->get($this->uri);

        if (!$this->isObject)
            return $roles;

        if (!isset($roles->data)) {
            $this->info = $roles;

            return $this;
        }

        return $this->formatedItemsClass($roles, 'roles');
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
