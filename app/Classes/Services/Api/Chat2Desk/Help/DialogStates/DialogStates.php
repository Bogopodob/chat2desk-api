<?php


namespace App\Classes\Services\Api\Chat2Desk\Help\DialogStates;


use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Help\Help;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class DialogStates extends Help implements Chat2DeskInterface {
    use FormatedClassTraids;

    /**
     * @var string
     */
    protected string $uri = '/dialog_states';

    /**
     * @var string
     */
    private string $paramsUri = '';

    public function getDialogStates() {
        $this->dialogStates = [];

        $this->uri = self::$baseUri . $this->uri;

        $dialogStates = $this->get($this->uri);

        if (!$this->isObject)
            return $dialogStates;

        if (!isset($dialogStates->data)) {
            $this->info = $dialogStates;

            return $this;
        }

        return $this->formatedItemsClass($dialogStates, 'dialogStates');
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
