<?php


namespace App\Classes\Services\Api\Chat2Desk\Help\ApiMode;


use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Help\Help;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class ApiMode extends Help implements Chat2DeskInterface {

    use FormatedClassTraids;

    /**
     * @var string
     */
    protected string $uri = '/api_modes';

    /**
     * @var string
     */
    private string $paramsUri = '';

    /**
     * @return $this
     * @throws \Exception
     */
    public function getApiMode () {
        $this->apiMode = [];

        $this->uri = self::$baseUri . $this->uri;

        $apiMode = $this->get($this->uri);

        if (!$this->isObject)
            return $apiMode;

        if (!isset($apiMode->data)) {
            $this->info = $apiMode;

            return $this;
        }

        return $this->formatedItemsClass($apiMode, 'apiMode');
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
