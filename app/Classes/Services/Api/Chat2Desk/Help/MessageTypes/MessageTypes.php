<?php


namespace App\Classes\Services\Api\Chat2Desk\Help\MessageTypes;


use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Help\Help;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class MessageTypes extends Help implements Chat2DeskInterface {
    use FormatedClassTraids;

    protected string $uri = '/message_types';

    /**
     * @var string
     */
    private string $paramsUri = '';

    public function getMessageTypes() {
        $this->messageTypes = [];

        $this->uri = self::$baseUri . $this->uri;

        $messageTypes = $this->get($this->uri);

        if (!$this->isObject)
            return $messageTypes;

        if (!isset($messageTypes->data)) {
            $this->info = $messageTypes;

            return $this;
        }

        return $this->formatedItemsClass($messageTypes, 'messageTypes');
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
