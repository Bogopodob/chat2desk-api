<?php


namespace App\Classes\Services\Api\Chat2Desk\Help\Transports;


use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Help\Help;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class Transports extends Help implements Chat2DeskInterface {
    use FormatedClassTraids;

    protected string $uri = '/transports';

    /**
     * @var string
     */
    private string $paramsUri = '';

    public function getTransports() {
        $this->transports = [];

        $this->uri = self::$baseUri . $this->uri;

        $transports = $this->get($this->uri);

        if (!$this->isObject)
            return $transports;

        if (!isset($transports->data)) {
            $this->info = $transports;

            return $this;
        }

        return $this->formatedItemsClass($transports, 'transports');
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
