<?php


namespace App\Classes\Services\Api\Chat2Desk\Help\Factory;


use App\Classes\Services\Api\Chat2Desk\Help\ApiMode\ApiMode;
use App\Classes\Services\Api\Chat2Desk\Help\DialogStates\DialogStates;
use App\Classes\Services\Api\Chat2Desk\Help\DialogTypes\DialogTypes;
use App\Classes\Services\Api\Chat2Desk\Help\MessageTypes\MessageTypes;
use App\Classes\Services\Api\Chat2Desk\Help\Roles\Roles;
use App\Classes\Services\Api\Chat2Desk\Help\Transports\Transports;

class HelpManager {

    /**
     * @var ApiMode
     */
    public ApiMode $apiMode;
    public DialogStates $dialogStates;
    public MessageTypes $messageTypes;
    public Roles $roles;
    public Transports $transports;

    public function __construct () {
        $this->apiMode = new ApiMode();
        $this->dialogStates = new DialogStates();
        $this->messageTypes = new MessageTypes();
        $this->roles = new Roles();
        $this->transports = new Transports();
    }
}
