<?php


namespace App\Classes\Services\Api\Chat2Desk\Help;


use App\Classes\Services\Api\Chat2Desk\Chat2Desk;

class Help extends Chat2Desk {
    protected static string $baseUri = '/help';
}
