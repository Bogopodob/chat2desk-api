<?php


namespace App\Classes\Services\Api\Chat2Desk\Factory;


use App\Classes\Services\Api\Chat2Desk\Channel\Builder\ChannelBuilder;
use App\Classes\Services\Api\Chat2Desk\Client\Builder\ClientBuilder;
use App\Classes\Services\Api\Chat2Desk\Companies\Builder\CompanyBuilder;
use App\Classes\Services\Api\Chat2Desk\Dialog\Builder\DialogBuilder;
use App\Classes\Services\Api\Chat2Desk\Help\Factory\HelpManager;
use App\Classes\Services\Api\Chat2Desk\Lists\Contact\Builder\CountryBuilder;
use App\Classes\Services\Api\Chat2Desk\Lists\Contact\Builder\RegionBuilder;
use App\Classes\Services\Api\Chat2Desk\Message\Builder\MessageBuilder;
use App\Classes\Services\Api\Chat2Desk\Other\Factory\OtherManager;

class Chat2DeskManager {

    /**
     * @var ClientBuilder
     */
    public ClientBuilder $client;

    /**
     * @var CountryBuilder
     */
    public CountryBuilder $country;

    /**
     * @var RegionBuilder
     */
    public RegionBuilder $region;

    /**
     * @var HelpManager
     */
    public HelpManager $help;

    /**
     * @var ChannelBuilder
     */
    public ChannelBuilder $channel;

    /**
     * @var MessageBuilder
     */
    public MessageBuilder $message;

    /**
     * @var DialogBuilder
     */
    public DialogBuilder $dialog;

    /**
     * @var CompanyBuilder
     */
    public CompanyBuilder $company;

    /**
     * @var OtherManager
     */
    public OtherManager $other;

    public function __construct () {
        $this->client = new ClientBuilder();
        $this->country = new CountryBuilder();
        $this->region = new RegionBuilder();
        $this->help =   new HelpManager();
        $this->channel = new ChannelBuilder();
        $this->dialog = new DialogBuilder();
        $this->message = new MessageBuilder();
        $this->company = new CompanyBuilder();
        $this->other = new OtherManager();
    }
}
