<?php


namespace App\Classes\Services\Api\Chat2Desk\Other\Statistics\Builder;


use App\Classes\Services\Api\Chat2Desk\Other\Statistics\Statistics;

class StatisticsBuilder implements StatisticsBuilderInterface {

    /**
     * @var array
     */
    private array $filter = [];

    /**
     * @var array
     */
    private array $params = [];

    /**
     * @var Statistics
     */
    protected Statistics $statistics;

    public function __construct () {
        $this->create();
    }

    public function create () : StatisticsBuilderInterface {
        $this->statistics = new Statistics();

        return $this;
    }

    /**
     * @param string $method
     * @param        $data
     * @return $this
     * @throws \Exception
     */
    public function setFilter (string $method, $data = '') : self {
        $this->filterValidate($method, $data);

        return $this;
    }

    /**
     * https://documenter.getpostman.com/view/9553101/SW7gSQAM#a9125895-d7a7-49b7-85fb-bc598c178dd7
     * Фильтр
     * methods:
     *   Тип данных ($data)   Метод  ($method)      Обязательные данные         Описания
     *  `string`              **report**            +Не обязательно+            - Тип отчета
     *  `int|string`          **date**              +Не обязательно+            - Дата
     *
     * @throws \Exception
     */
    private function filterValidate (string $method, $data = '') : void {
        switch ($method) {
            case 'report':
            {
                $this->filter[$method] = $data;
                break;
            }
            case 'date': {
                if (is_int($data)) {
                    $this->filter[$method] = \date('d-m-Y', strtotime($data));
                    break;
                }

                else if (is_string($data)) {
                    if (!preg_match('/(?:\d{4}|\d{2})(?:\.|\-|\/)\d{2}(?:\.|\-|\/)(?:\d{4}|\d{2})/ui', $data))
                        throw new \Exception("This method has an unacceptable date format, for example: (" . \date('Y-m-d') . ' or ' . \date('d-m-Y') . "). Symbols: (-|.|/), $method");

                    $this->filter[$method] = $data;
                    break;
                }

                throw new \Exception('The value must be a string or a number.');
            }
            default:
                throw new \Exception("Not filter method, $method");
        }
    }

    /**
     * @return Statistics
     */
    public function getStatistics () : Statistics {
        $statistics = $this->statistics;

        $statistics->filter         = $this->filter;

        $statistics->params = $this->params;

        $this->create();

        return $statistics;
    }
}
