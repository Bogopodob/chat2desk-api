<?php


namespace App\Classes\Services\Api\Chat2Desk\Other\Statistics\Builder;


interface StatisticsBuilderInterface {

    /**
     * @return StatisticsBuilderInterface
     */
    public function create() : StatisticsBuilderInterface;
}
