<?php


namespace App\Classes\Services\Api\Chat2Desk\Other\Statistics;


use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Other\Other;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class Statistics extends Other implements Chat2DeskInterface {
    use FormatedClassTraids;

    protected string $uri = '/statistics';

    /**
     * @var string
     */
    private string $paramsUri = '';

    public function getItems() {
        $this->statistic = null;

        $this->paramsUri = urldecode(http_build_query($this->filter));
        $statistic         = $this->get($this->paramsUri ? $this->uri . '?' . $this->paramsUri : $this->uri);

        if (!$this->isObject)
            return $statistic;

        return $this->formatedItemsClass($statistic, 'statistic');
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
