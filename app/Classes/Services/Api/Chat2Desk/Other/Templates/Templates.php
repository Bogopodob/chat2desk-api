<?php


namespace App\Classes\Services\Api\Chat2Desk\Other\Templates;


use App\Classes\Services\Api\Chat2Desk\Chat2DeskInterface;
use App\Classes\Services\Api\Chat2Desk\Other\Other;
use App\Classes\Services\Api\Chat2Desk\Traids\FormatedClassTraids;

class Templates extends Other implements Chat2DeskInterface {
    use FormatedClassTraids;

    /**
     * @var string
     */
    protected string $uri = '/templates';

    /**
     * @var string
     */
    private string $paramsUri = '';

    /**
     * @return $this
     * @throws \Exception
     */
    public function getItems () {
        $this->templates = [];

        $this->uri = self::$baseUri . $this->uri;

        $templates = $this->get($this->uri);

        if (!$this->isObject)
            return $templates;

        if (!isset($templates->data)) {
            $this->info = $templates;

            return $this;
        }

        return $this->formatedItemsClass($templates, 'templates');
    }

    /**
     * @return Templates|array|object
     * @throws \Exception
     */
    public function getItem() {
        if (!isset($this->id))
            throw new \Exception('Not a value in id, use ->setId(id)');

        $this->template = NULL;

        $this->filterValidateItem();
        $template       = $this->get($this->uri . '/' . $this->id);

        if (!$this->isObject)
            return $template;

        return $this->formatedItemClass($template, 'template', '/' . $this->id);
    }

    private function filterValidateItem() : void {
        unset($this->filter['limit'], $this->filter['offset']);
    }

    /**
     * @param bool $isObject
     * @return $this
     */
    public function toObject (bool $isObject) : self {
        $this->isObject = $isObject;
        return $this;
    }
}
