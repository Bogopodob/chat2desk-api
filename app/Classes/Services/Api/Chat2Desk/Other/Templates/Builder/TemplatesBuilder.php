<?php


namespace App\Classes\Services\Api\Chat2Desk\Other\Templates\Builder;


use App\Classes\Services\Api\Chat2Desk\Other\Templates\Templates;
use App\Classes\Services\Api\Chat2Desk\Traids\FilterTraids;

class TemplatesBuilder implements TemplatesBuilderInterface {

    use FilterTraids;

    /**
     * @var int|null
     */
    private ?int $id = null;

    /**
     * @var Templates
     */
    protected Templates $templates;

    public function __construct () {
        $this->create();
    }

    /**
     * @return $this
     */
    public function create () : self {
        $this->templates = new Templates();

        return $this;
    }

    /**
     * @param int|null $id
     * @return $this
     */
    public function setId (?int $id) : self {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Templates
     */
    public function getTemplates() : Templates {
        $templates = $this->templates;

        $this->filter['limit']  = $this->limit;
        $this->filter['offset'] = $this->offset;
        $templates->filter         = $this->filter;

        if ($this->id)
            $templates->id = $this->id;

        $this->create();

        return $templates;
    }
}
