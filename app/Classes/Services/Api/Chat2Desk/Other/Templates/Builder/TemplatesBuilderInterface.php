<?php


namespace App\Classes\Services\Api\Chat2Desk\Other\Templates\Builder;


interface TemplatesBuilderInterface {

    /**
     * @return TemplatesBuilderInterface
     */
    public function create() : TemplatesBuilderInterface;

    /**
     * @param int|null $id
     * @return TemplatesBuilderInterface
     */
    public function setId(?int $id) : TemplatesBuilderInterface;
}
