<?php


namespace App\Classes\Services\Api\Chat2Desk\Other\Factory;

use App\Classes\Services\Api\Chat2Desk\Other\Statistics\Builder\StatisticsBuilder;
use App\Classes\Services\Api\Chat2Desk\Other\Templates\Builder\TemplatesBuilder;

class OtherManager {
    public TemplatesBuilder $templates;
    public StatisticsBuilder $statistic;

    public function __construct () {
        $this->templates = new TemplatesBuilder();
        $this->statistic = new StatisticsBuilder();
    }
}
