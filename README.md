![Alt-текст](https://salesap.ru/assets/pages/integrations/logos/chat2desk-4927a3ffeed18c249c670612115e4fb68d5bb70e484b878db6b4f540bc92f076.png "Chat2Desk")
____
Приложения для работы с агрегатором мессенджеров *[chat2desk](https://chat2desk.com/)*.
Официальную документацию можно посмотреть в двух вариантах:
- **Обязательна нужна авторизация**, *[документация](https://web.chat2desk.com/wiki/api)* (Вход->справка->описание api).
- [Просмотр демонтрации через postman](https://documenter.getpostman.com/view/9553101/SW7gSQAM#intro). **Некоторые данные не в актуальном состоянии.**

Минимальные требования к приложению.
1. PHP >= 8.0.

Использования

```php
use \App\Classes\Services\Api\Chat2Desk\Chat2Desk;

$chat2Desk = new Chat2Desk();
$chat2Desk::setToken('Your token');

/* Не обязательно */
// $chat2Desk::setXBotMarketing('');
```
***Получения всех пользователей***
```php
$clients = $chat2Desk::getManager()
                       ->client
                       ->getClient()
                       ->getItems();
var_dump($clients);
```
***Получения всех пользователей с фильтрами (все фильтры можно посмотреть в классе ClientBuilder)***
```php
$clients = $chat2Desk::getManager()
                       ->client
                       ->setFilter('order', 'desc')
                       ->setFilter('offset', 50)
                       ->getClient()
                       ->getItems();
var_dump($clients);
```

***Получения конкретного пользователя***
```php
$clients = $chat2Desk::getManager()
                       ->client
                       ->setId('Id client')
                       ->getClient()
                       ->getItem();
var_dump($clients);
```

***Получения конкретного пользователя с фильтрами типа uri (все фильтры можно посмотреть в классе ClientBuilder)***
```php
$clients = $chat2Desk::getManager()
                       ->client
                       ->setFilter('last_question')
                       ->getClient()
                       ->getItem();
var_dump($clients);
```
***Все методы работают по одному принцепу***
```php
$data = $chat2Desk::getManager()
                       ->client->...
                       or 
                       ->message->...,
                       or
                       ->channel->...,
                       or
                       ->dialog->...,
                       or
                       ->help->apiMode->...              
```